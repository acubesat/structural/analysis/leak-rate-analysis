import math
from dataclasses import dataclass
from typing import Dict, NamedTuple

@dataclass
class GasketMaterial:
    name: str
    compression_modulus_mpa: float  
    min_compression_percent: float  
    max_compression_percent: float  
    creep_resistance: str  

@dataclass
class FlangeMaterial:
    name: str
    elastic_modulus_mpa: float
    poisson_ratio: float

class BoltSpec(NamedTuple):
    nominal_diameter_mm: float
    stress_area_mm2: float
    pitch_mm: float
    yield_strength_mpa: float
    tensile_strength_mpa: float
    k_factor: float
    rec_torque_nm: float

class GasketGeometry(NamedTuple):
    outer_dim_mm: float
    inner_dim_mm: float
    initial_thickness_mm: float

def calculate_flange_stiffness(
    gasket_geometry: GasketGeometry,
    flange_material: FlangeMaterial,
    flange_thickness_mm: float
) -> float:
    """
    Calculate flange stiffness using plate bending theory (for a square flange)
    Returns stiffness in N/mm
    """
    # For a square flange, use the side length as characteristic dimension
    # Could also use diagonal = side_length * √2 for more conservative estimate
    characteristic_length = gasket_geometry.outer_dim_mm

    # Plate stiffness calculation (simplified model)
    # Using square plate bending with concentrated load
    E = flange_material.elastic_modulus_mpa
    v = flange_material.poisson_ratio
    t = flange_thickness_mm
    L = characteristic_length
    
    # Simplified plate stiffness (based on concentrated load at center)
    # This is a approximation - real behavior is more complex
    stiffness = (16 * E * t**3) / (12 * (1 - v**2) * L**2)
    
    return stiffness

def calculate_system_compression(
    gasket_geometry: GasketGeometry,
    bolt_spec: BoltSpec,
    num_bolts: int,
    gasket_material: GasketMaterial,
    flange_material: FlangeMaterial,
    flange_thickness_mm: float
) -> Dict:
    """
    Calculate compression considering both gasket and flange elasticity
    """

    # Calculate gasket area
    outer_area_mm2 = gasket_geometry.outer_dim_mm ** 2
    inner_area_mm2 = gasket_geometry.inner_dim_mm ** 2
    gasket_area_mm2 = outer_area_mm2 - inner_area_mm2
    gasket_area_m2 = gasket_area_mm2 * 1e-6

    # Calculate total bolt force
    force_per_bolt = bolt_spec.rec_torque_nm / (bolt_spec.k_factor * bolt_spec.nominal_diameter_mm / 1000)
    total_force = force_per_bolt * num_bolts

    # Calculate stiffnesses
    gasket_stiffness = (gasket_material.compression_modulus_mpa * 1e6 * gasket_area_m2) / (gasket_geometry.initial_thickness_mm)
    flange_stiffness = calculate_flange_stiffness(gasket_geometry, flange_material, flange_thickness_mm)
    
    # Combined system stiffness (springs in series)
    # 1/k_total = 1/k_gasket + 1/k_flange1 + 1/k_flange2
    system_stiffness = 1 / (1/gasket_stiffness + 1/flange_stiffness + 1/flange_stiffness)
    
    # Calculate total displacement
    total_displacement_mm = total_force / system_stiffness
    
    # Calculate gasket compression (portion of total displacement)
    gasket_compression_mm = total_displacement_mm * (system_stiffness / gasket_stiffness)
    compression_percentage = (gasket_compression_mm / gasket_geometry.initial_thickness_mm) * 100
    
    # Calculate actual pressure on gasket
    pressure_mpa = (total_force * (system_stiffness / gasket_stiffness)) / gasket_area_m2

    # Evaluate compression against recommendations
    compression_status = (
        "OK"
        if gasket_material.min_compression_percent <= compression_percentage <= gasket_material.max_compression_percent
        else "Too Low"
        if compression_percentage < gasket_material.min_compression_percent
        else "Too High"
    )

    return {
        "material_name": gasket_material.name,
        "gasket_area_mm2": round(gasket_area_mm2, 2),
        "total_force_n": round(total_force, 2),
        "system_stiffness_n_mm": round(system_stiffness, 2),
        "flange_stiffness_n_mm": round(flange_stiffness, 2),
        "gasket_stiffness_n_mm": round(gasket_stiffness, 2),
        "total_displacement_mm": round(total_displacement_mm, 3),
        "gasket_compression_mm": round(gasket_compression_mm, 3),
        "compression_percentage": round(compression_percentage, 2),
        "pressure_mpa": round(pressure_mpa, 2),
        "compression_status": compression_status,
        "creep_resistance": gasket_material.creep_resistance
    }

# Materials database
materials = {
    "PTFE": GasketMaterial(
        name="PTFE",
        compression_modulus_mpa=600,
        min_compression_percent=15,
        max_compression_percent=25,
        creep_resistance="Poor - may require frequent retightening"
    ),
    "VITON": GasketMaterial(
        name="Viton",
        compression_modulus_mpa=150,
        min_compression_percent=15,
        max_compression_percent=50,
        creep_resistance="Good"
    ),
    "SILICONE": GasketMaterial(
        name="Silicone",
        compression_modulus_mpa=100,
        min_compression_percent=15,
        max_compression_percent=50,
        creep_resistance="Fair"
    ),
    "NBR": GasketMaterial(
        name="NBR",
        compression_modulus_mpa=120,
        min_compression_percent=15,
        max_compression_percent=50,
        creep_resistance="Good"
    )
}

# Aluminum 6061 properties
flange = FlangeMaterial(
    name="Aluminum 6061",
    elastic_modulus_mpa=69000,  # ~69 GPa
    poisson_ratio=0.33
)

# Run analysis
if __name__ == "__main__":
    thicknesses = [2, 3, 4]  # mm
    
    bolt = BoltSpec(
        nominal_diameter_mm=4.0,
        stress_area_mm2=8.78,
        pitch_mm=0.7,
        yield_strength_mpa=450,
        tensile_strength_mpa=700,
        k_factor=0.3,
        rec_torque_nm=2.3
    )

    num_bolts = 16
    flange_thickness_mm = 6

    print("\nGasket Compression Analysis - Thickness Comparison")
    print("-" * 95)
    
    # Print header
    print(f"{'Material':<10} {'Initial':<10} {'Final':<10} {'Compression':<15} {'Pressure':<12} {'Status':<10}")
    print(f"{'':10} {'(mm)':<10} {'(mm)':<10} {'(%)':<15} {'(MPa)':<12} {'':<10}")
    print("-" * 95)
    
    for material_name, material in materials.items():
        for thickness in thicknesses:
            gasket = GasketGeometry(
                outer_dim_mm=81.46,
                inner_dim_mm=75.35,
                initial_thickness_mm=thickness
            )
            
            result = calculate_system_compression(
                gasket, bolt, num_bolts, material, flange, flange_thickness_mm
            )
            
            final_thickness = thickness - result['gasket_compression_mm']
            
            print(
                f"{material_name:<10} "
                f"{thickness:>5.1f}      "
                f"{final_thickness:>5.2f}      "
                f"{result['compression_percentage']:>6.1f}        "
                f"{result['pressure_mpa']:>6.1f}      "
                f"{result['compression_status']:<10}"
            )
        print("-" * 95)  # Separator between materials

    print("\nDetailed Stiffness Analysis")
    print("-" * 80)
    print(f"{'Material':<10} {'Thickness':<10} {'System':<15} {'Flange':<15} {'Gasket':<15}")
    print(f"{'':10} {'(mm)':<10} {'(N/mm)':<15} {'(N/mm)':<15} {'(N/mm)':<15}")
    print("-" * 80)
    
    for material_name, material in materials.items():
        for thickness in thicknesses:
            gasket = GasketGeometry(
                outer_dim_mm=81.46,
                inner_dim_mm=75.35,
                initial_thickness_mm=thickness
            )
            
            result = calculate_system_compression(
                gasket, bolt, num_bolts, material, flange, flange_thickness_mm
            )
            
            print(
                f"{material_name:<10} "
                f"{thickness:>5.1f}      "
                f"{result['system_stiffness_n_mm']:>6.0f}        "
                f"{result['flange_stiffness_n_mm']:>6.0f}        "
                f"{result['gasket_stiffness_n_mm']:>6.0f}"
            )
        print("-" * 80)  # Separator between materials

    # Add summary of recommendations
    print("\nRecommendations Summary:")
    print("-" * 80)
    for material_name, material in materials.items():
        best_thickness = None
        best_status = "Too Low"
        best_compression = 0
        
        for thickness in thicknesses:
            gasket = GasketGeometry(
                outer_dim_mm=81.46,
                inner_dim_mm=75.35,
                initial_thickness_mm=thickness
            )
            
            result = calculate_system_compression(
                gasket, bolt, num_bolts, material, flange, flange_thickness_mm
            )
            
            if result['compression_status'] == "OK":
                if best_thickness is None or abs(result['compression_percentage'] - (material.min_compression_percent + material.max_compression_percent)/2) < abs(best_compression - (material.min_compression_percent + material.max_compression_percent)/2):
                    best_thickness = thickness
                    best_compression = result['compression_percentage']
                    best_status = "OK"
        
        if best_thickness:
            print(f"{material_name}: Best initial thickness is {best_thickness}mm (achieves {best_compression:.1f}% compression)")
        else:
            print(f"{material_name}: No thickness provides optimal compression - consider adjusting bolt torque")