import pandas as pd
import matplotlib.pyplot as plt
from dataclasses import dataclass
from typing import Dict, List, Optional
from datetime import datetime

@dataclass
class GasketMaterial:
    name: str
    min_seating_stress: float  # Y value (MPa)
    gasket_factor: float  # M value (dimensionless)
    compression_range: tuple[float, float]  # min, max percentage

class GasketCalculationError(Exception):
    """Custom exception for gasket calculation errors"""
    pass

# Gasket material properties
GASKET_MATERIALS = {
    'VITON': GasketMaterial('VITON', 1.4, 1.0, (15, 50)),
    'SILICON': GasketMaterial('Silicon', 1.4, 1.0, (15, 50)),
    'PTFE': GasketMaterial('PTFE', 16.20268, 2.0, (15, 25)),
    'NBR': GasketMaterial('NBR', 1.4, 1.0, (15, 50))
}

# A2-70 bolt properties with ECSS-E-HB-32-23A specifications
BOLT_PROPERTIES = {
    'M4': {
        'nominal_diameter_mm': 4.0,
        'stress_area_mm2': 8.78,
        'pitch_mm': 0.7,
        'yield_strength_mpa': 450,
        'tensile_strength_mpa': 700,
        'k_factor': 0.3,
        'rec_torque_nm': 2.3,
        'standard': 'ECSS-E-HB-32-23A Annex C Table C-2'
    }
}

def validate_inputs(
    outer_dim_mm: float,
    inner_dim_mm: float,
    initial_thickness_mm: float,
    final_thickness_mm: float,
    material: str,
    num_bolts: int
) -> None:
    """Validate input parameters"""
    if outer_dim_mm <= inner_dim_mm:
        raise GasketCalculationError("Outer dimension must be greater than inner dimension")
    if initial_thickness_mm <= final_thickness_mm:
        raise GasketCalculationError("Initial thickness must be greater than final thickness")
    if material not in GASKET_MATERIALS:
        raise GasketCalculationError(f"Unknown material: {material}. Available materials: {list(GASKET_MATERIALS.keys())}")
    if num_bolts <= 0:
        raise GasketCalculationError("Number of bolts must be positive")

def calculate_gasket_parameters(
    outer_dim_mm: float,
    inner_dim_mm: float,
    initial_thickness_mm: float,
    final_thickness_mm: float,
    material: str,
    num_bolts: int,
    system_pressure_mpa: float = 0.12,
    safety_factor: float = 0.75
) -> Dict:
    """Calculate complete gasket assembly parameters with ECSS bolt standards"""
    
    # Validate inputs
    validate_inputs(outer_dim_mm, inner_dim_mm, initial_thickness_mm, 
                   final_thickness_mm, material, num_bolts)
    
    # Convert dimensions to areas
    outer_area_mm2 = outer_dim_mm ** 2
    inner_area_mm2 = inner_dim_mm ** 2
    gasket_area_mm2 = outer_area_mm2 - inner_area_mm2
    
    # Get material properties
    gasket_material = GASKET_MATERIALS[material]
    bolt_props = BOLT_PROPERTIES['M4']
    
    # Calculate compression percentage
    compression_percentage = ((initial_thickness_mm - final_thickness_mm) / initial_thickness_mm) * 100
    
    # Calculate forces
    seating_force_n = gasket_area_mm2 * gasket_material.min_seating_stress
    operating_force_n = (system_pressure_mpa * gasket_area_mm2 * 
                        gasket_material.gasket_factor)
    
    # Total required bolt force (use maximum of seating and operating)
    total_bolt_force_n = max(seating_force_n, operating_force_n)
    force_per_bolt_n = total_bolt_force_n / num_bolts
    
    # Calculate bolt stress
    bolt_stress_mpa = force_per_bolt_n / bolt_props['stress_area_mm2']
    
    # Calculate torque (T = K × F × d)
    calculated_torque_nm = (bolt_props['k_factor'] * force_per_bolt_n * 
                          bolt_props['nominal_diameter_mm'] / 1000)
    
    # Set torque to ECSS recommended one
    recommended_torque_nm = bolt_props['rec_torque_nm']
    
    # Check if design is within safe limits
    max_allowed_stress = bolt_props['yield_strength_mpa'] * safety_factor
    is_bolt_safe = bolt_stress_mpa < max_allowed_stress
    
    is_compression_safe = (gasket_material.compression_range[0] <= 
                          compression_percentage <= 
                          gasket_material.compression_range[1])
    
    # Add torque safety check
    is_torque_safe = calculated_torque_nm <= bolt_props['rec_torque_nm']
    
    return {
        "Material Properties": {
            "Material": material,
            "Min Seating Stress (MPa)": gasket_material.min_seating_stress,
            "Gasket Factor": gasket_material.gasket_factor,
            "Safe Compression Range (%)": gasket_material.compression_range
        },
        "Dimensions": {
            "Outer Dimension (mm)": outer_dim_mm,
            "Inner Dimension (mm)": inner_dim_mm,
            "Initial Thickness (mm)": initial_thickness_mm,
            "Final Thickness (mm)": final_thickness_mm,
            "Gasket Area (mm²)": round(gasket_area_mm2, 2),
            "Compression (%)": round(compression_percentage, 1)
        },
        "Forces": {
            "Seating Force (N)": round(seating_force_n, 2),
            "Operating Force (N)": round(operating_force_n, 2),
            "Total Bolt Force (N)": round(total_bolt_force_n, 2),
            "Force per Bolt (N)": round(force_per_bolt_n, 2)
        },
        "Bolt Calculations": {
            "Bolt Standard": bolt_props['standard'],
            "Friction Coefficient": bolt_props['k_factor'],
            "Bolt Stress (MPa)": round(bolt_stress_mpa, 2),
            "Calculated Torque (N⋅m)": round(calculated_torque_nm, 3),
            "ECSS Max Torque (N⋅m)": bolt_props['rec_torque_nm'],
            "Recommended Torque (N⋅m)": round(recommended_torque_nm, 3),
            "Recommended Torque (N⋅mm)": round(recommended_torque_nm * 1000, 1)
        },
        "Safety Checks": {
            "Is Bolt Stress Safe": is_bolt_safe,
            "Is Compression Safe": is_compression_safe,
            "Is Torque Within ECSS Limit": is_torque_safe,
            "Max Allowed Stress (MPa)": round(max_allowed_stress, 2),
            "Stress Utilization (%)": round((bolt_stress_mpa / max_allowed_stress) * 100, 1)
        }
    }

def generate_comparison_table():
    """
    Generate comparison table for all materials and thicknesses
    """
    results = []
    thicknesses = [2, 3, 4]
    
    for material in GASKET_MATERIALS.keys():
        for thickness in thicknesses:
            result = calculate_gasket_parameters(
                outer_dim_mm=81.46,
                inner_dim_mm=75.35,
                initial_thickness_mm=thickness,
                final_thickness_mm=1.6,
                material=material,
                num_bolts=16
            )
            
            results.append({
                "Material": material,
                "Initial Thickness (mm)": thickness,
                "Compression (%)": result["Dimensions"]["Compression (%)"],
                "Force per Bolt (N)": result["Forces"]["Force per Bolt (N)"],
                "Calc. Torque (N⋅m)": result["Bolt Calculations"]["Calculated Torque (N⋅m)"],
                "Rec. Torque (N⋅m)": result["Bolt Calculations"]["Recommended Torque (N⋅m)"],
                "Bolt Stress (MPa)": result["Bolt Calculations"]["Bolt Stress (MPa)"],
                "Stress Utilization (%)": result["Safety Checks"]["Stress Utilization (%)"],
                "Is Bolt Stress Safe": result["Safety Checks"]["Is Bolt Stress Safe"],
                "Is Compression Safe": result["Safety Checks"]["Is Compression Safe"],
                "Is Torque Safe": result["Safety Checks"]["Is Torque Within ECSS Limit"]
            })
    
    return pd.DataFrame(results)

def main():
    # Generate and display comparison table
    comparison_table = generate_comparison_table()
    print("\nComparison Table:")
    print(comparison_table.to_string(index=False))

    # Calculation example for one case
    example = calculate_gasket_parameters(
        outer_dim_mm=81.46,
        inner_dim_mm=75.35,
        initial_thickness_mm=2,
        final_thickness_mm=1.6,
        material='VITON',
        num_bolts=16
    )

    print("\nDetailed Example for VITON 2mm:")
    for category, values in example.items():
        print(f"\n{category}:")
        for key, value in values.items():
            print(f"  {key}: {value}")

if __name__ == "__main__":
    main()